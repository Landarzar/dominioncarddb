package net.landarzar.dominion.carddb;

public enum Typ
{
	ANGRIFF,
	AKTION,
	GELD,
	PUNKTE,
	REAKTION,
	UNTERSCHLUPF,
	RITTER,
	PLÜNDERN,
	PREIS,
	DAUER
}
