package net.landarzar.dominion.carddb;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.Html.ImageGetter;
import android.text.style.ForegroundColorSpan;

public class CardDisplay extends Activity
{

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.activity_card_display);
		Intent intent = getIntent();
		Card card = (Card) intent.getSerializableExtra(MainActivity.EXTRA_CARD);
		
		ImageGetter getter = new ResImageGetter(getResources());

		this.setTitle(card.getName());

		TextView kostenText = (TextView) findViewById(R.id.textKosten);
		String kosten = (card.getTrank() <= 0 || card.getCost() > 0 ? "<img src=\"geld" + card.getCost() + ".png\" />" : "") + (card.getTrank() <= 0 ? "" : "<img src=\"trank.png\" />");
		Spanned sp = Html.fromHtml(kosten, new ResImageGetter(getResources(), 25f), null);
		kostenText.setText(sp, TextView.BufferType.SPANNABLE);

		String s = "";
		for (Typ t : card.getTypes())
		{
			s += t.toString() + " ";
		}

		TextView kinds = (TextView) findViewById(R.id.textKinds);
		kinds.setText(s);

		TextView kartentext = (TextView) findViewById(R.id.textKartenText);
		sp = Html.fromHtml(card.getText(), getter, null);
		kartentext.setText(sp, TextView.BufferType.SPANNABLE);

		TextView edition = (TextView) findViewById(R.id.textEdition);
		edition.setText(card.getEdition().toString());

		TextView regeln = (TextView) findViewById(R.id.textRegel);
		sp = Html.fromHtml(card.getDescription(), getter, null);
		regeln.setText(sp, TextView.BufferType.SPANNABLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_card_display, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
