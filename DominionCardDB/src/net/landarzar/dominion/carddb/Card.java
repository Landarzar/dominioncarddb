package net.landarzar.dominion.carddb;

import java.io.Serializable;

public class Card implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5510295292557007445L;
	private Edition edition;
	private int cost;
	private int trank;
	private String name;
	private String text;
	private String description;
	private Typ[] types = new Typ[0];

	public Card()
	{
	}

	/**
	 * @return the edition
	 */
	public Edition getEdition()
	{
		return edition;
	}

	/**
	 * @param edition
	 *            the edition to set
	 */
	public void setEdition(Edition edition)
	{
		this.edition = edition;
	}
	
	/**
	 * @param edition
	 *            the edition to set
	 */
	public void SetTypes(Typ[] types)
	{
		this.types = types;
	}

	/**
	 * @return the cost
	 */
	public int getCost()
	{
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(int cost)
	{
		this.cost = cost;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * @return the name
	 */
	public Typ[] getTypes()
	{
		return types;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text)
	{
		this.text = text;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		
		String s = "";
		
		for(Typ t : types)
		{
			s += t + " ";
		}
		
		return name;
	}

	public int getTrank()
	{
		return trank;
	}

	public void setTrank(int trank)
	{
		this.trank = trank;
	}
}
