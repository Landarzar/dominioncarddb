package net.landarzar.dominion.carddb;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Html.ImageGetter;
import android.util.DisplayMetrics;

public class ResImageGetter implements ImageGetter
{
	public ResImageGetter(Resources res)
	{
		this.res = res;
	}
	public ResImageGetter(Resources res, float size)
	{
		this.res = res;
		this.size = size;
	}
	
	private float size = 16.5f;
	private Resources res;

	public Drawable getDrawable(String source)
	{
		Drawable d = null;
		if (source.equals("geld.png"))
			d = res.getDrawable(R.drawable.geld);
		else if (source.equals("geld0.png"))
			d = res.getDrawable(R.drawable.geld0);
		else if (source.equals("geld0star.png"))
			d = res.getDrawable(R.drawable.geld0star);
		else if (source.equals("geld1.png"))
			d = res.getDrawable(R.drawable.geld1);
		else if (source.equals("geld2.png"))
			d = res.getDrawable(R.drawable.geld2);
		else if (source.equals("geld3.png"))
			d = res.getDrawable(R.drawable.geld3);
		else if (source.equals("geld4.png"))
			d = res.getDrawable(R.drawable.geld4);
		else if (source.equals("geld5.png"))
			d = res.getDrawable(R.drawable.geld5);
		else if (source.equals("geld6.png"))
			d = res.getDrawable(R.drawable.geld6);
		else if (source.equals("geld7.png"))
			d = res.getDrawable(R.drawable.geld7);
		else if (source.equals("geld8.png"))
			d = res.getDrawable(R.drawable.geld8);
		else if (source.equals("geld9.png"))
			d = res.getDrawable(R.drawable.geld9);
		else if (source.equals("trank.png"))
			d = res.getDrawable(R.drawable.trank);
		else if (source.equals("wappen.png"))
			d = res.getDrawable(R.drawable.wappen);
		if (d != null)
			d.setBounds(0, 0, (int) convertDpToPixel(size, res), (int) convertDpToPixel(size, res));
		return d;
	}
	

	public static float convertPixelsToDp(float px, Resources resources)
	{
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;

	}

	public static float convertDpToPixel(float dp, Resources resources)
	{
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}
}
