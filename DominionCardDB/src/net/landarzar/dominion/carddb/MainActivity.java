package net.landarzar.dominion.carddb;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.os.Bundle;
import android.R.anim;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener
{
	public final static String EXTRA_CARD = "net.landarzar.dominion.carddb.EXTRA_CARD";
	private ArrayAdapter<Card> adapter = null;
	private List<Card> cards = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		// getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_alert);
		setContentView(R.layout.activity_main);
		ListView listView = (ListView) findViewById(R.id.listView1);

		listView.setOnItemClickListener(this);

		adapter = new ArrayAdapter<Card>(this, android.R.layout.simple_list_item_1);
		listView.setAdapter(adapter);

		EditText editText = (EditText) findViewById(R.id.edit_message);

		editText.addTextChangedListener(new TextWatcher()
		{

			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			public void afterTextChanged(Editable s)
			{
				EditText editText = (EditText) findViewById(R.id.edit_message);
				String message = editText.getText().toString();

				List<Card> filtered = new LinkedList<Card>();
				for (Card card : cards)
				{
					if (card.getName().toLowerCase().contains(message.toString().toLowerCase()))
						filtered.add(card);
				}

				adapter.clear();

				Collections.sort(filtered, new Comparator<Card>()
				{

					public int compare(Card lhs, Card rhs)
					{
						return lhs.toString().compareTo(rhs.toString());
					}
				});

				for (Card card : filtered)
				{
					adapter.add(card);
				}
			}
		});

		parseCards();

		for (Card card : cards)
		{
			adapter.add(card);
		}
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		Intent intent = new Intent(this, CardDisplay.class);
		ListView listView = (ListView) findViewById(R.id.listView1);

		Card card = (Card) listView.getItemAtPosition(position);

		intent.putExtra(EXTRA_CARD, card);

		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private void parseCards()
	{
		Resources res = getResources();
		cards = new LinkedList<Card>();

		XmlResourceParser xpp = res.getXml(R.xml.dominion);

		Card current = null;
		StringBuilder builder = new StringBuilder();

		int eventType;
		try
		{
			eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT)
			{
				if (eventType == XmlPullParser.START_DOCUMENT)
				{
					System.out.println("Start document");
				} else if (eventType == XmlPullParser.START_TAG)
				{
					String qName = xpp.getName();
					if (qName.equals("Card"))
					{
						current = new Card();
						for (int i = 0; i < xpp.getAttributeCount(); i++)
						{
							String aName = xpp.getAttributeName(i);
							if (aName.equals("Name"))
							{
								current.setName(xpp.getAttributeValue(i));
								System.out.println("Get: " + xpp.getAttributeValue(i));
							}
							if (aName.equals("Cost"))
								current.setCost(xpp.getAttributeIntValue(i, -1));
							if (aName.equals("Trank"))
								current.setTrank(xpp.getAttributeIntValue(i, -1));
							if (aName.equals("Kind"))
							{
								String[] ts = xpp.getAttributeValue(i).split(" ");
								Typ[] arr = new Typ[ts.length];
								for (int j = 0; j < ts.length; ++j)
								{
									arr[j] = Typ.valueOf(ts[j]);
								}
								current.SetTypes(arr);
							}
							if (aName.equals("Edition"))
								current.setEdition(Edition.valueOf(xpp.getAttributeValue(i).trim()));
						}
					} else if (qName.equals("Text") || qName.equals("Description"))
					{
						builder = new StringBuilder();
					}

				} else if (eventType == XmlPullParser.END_TAG)
				{
					String qName = xpp.getName();
					if (qName.equals("Card"))
					{
						cards.add(current);
					} else if (qName.equals("Text"))
					{
						current.setText(builder.toString());
					} else if (qName.equals("Description"))
					{
						current.setDescription(builder.toString());
					}
				} else if (eventType == XmlPullParser.TEXT)
				{
					String text = xpp.getText();
					text = text.replace("\t", "");
					text = text.replace("\n", " ");
					text = text.replace("  ", " ");
					text = text.replace("(W)", "<img src=\"wappen.png\" />");
					text = text.replace("(T)", "<img src=\"trank.png\" />");
					text = text.replace("(G)", "<img src=\"geld.png\" />");
					text = text.replace("(0)", "<img src=\"geld0.png\" />");
					text = text.replace("(1)", "<img src=\"geld1.png\" />");
					text = text.replace("(2)", "<img src=\"geld2.png\" />");
					text = text.replace("(3)", "<img src=\"geld3.png\" />");
					text = text.replace("(4)", "<img src=\"geld4.png\" />");
					text = text.replace("(5)", "<img src=\"geld5.png\" />");
					text = text.replace("(6)", "<img src=\"geld6.png\" />");
					text = text.replace("(7)", "<img src=\"geld7.png\" />");
					text = text.replace("(8)", "<img src=\"geld8.png\" />");
					text = text.replace("(9)", "<img src=\"geld9.png\" />");
					text = text.replace("---", "<br/><hr/><br/>");

					builder.append(text);
				}
				eventType = xpp.next();
			}
		} catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Collections.sort(cards, new Comparator<Card>()
		{

			public int compare(Card lhs, Card rhs)
			{
				return lhs.toString().compareTo(rhs.toString());
			}
		});
	}
}
