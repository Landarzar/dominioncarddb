package net.landarzar.dominion.carddb;

public enum Edition
{
	Basis,
	Seaside,
	DarkAges,
	Intrige,
	Blütezeit,
	Hinterland,
	ReicheErnte,
	Alchemist
}
